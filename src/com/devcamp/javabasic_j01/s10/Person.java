package com.devcamp.javabasic_j01.s10;

public class Person {
    public String name;
    protected String address;

    public Person() {
        this.name = "Tran NGOC My Hoa";
        this.address = "K48 Quy Nhon";
    }

    public Person(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person [name=" + name + ", address=" + address + "]";
    }

}
