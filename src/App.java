import com.devcamp.javabasic_j01.s10.Person;
import com.devcamp.javabasic_j01.s10.Staff;
import com.devcamp.javabasic_j01.s10.Student;

public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person();
        Person person2 = new Person("tran hoai an", "Chua Lang, Ha Noi");
        System.out.println(person1);
        System.out.println(person2);

        Student student1 = new Student("Ngo thi hoangf nghia", "DIEN NGOC", "IT", 2022, 200000);
        Student student2 = new Student("Ngo ", "NGOC", "art", 2022, 10000000);
        Student student3 = new Student();
        System.out.println(student1);
        System.out.println(student2);
        System.out.println(student3);

        Staff staff1 = new Staff();
        Staff staff2 = new Staff("Toan Dinh", "HDUONG", "THPT CHUYEN LE THANH TONG", 200000);
        Staff staff3 = new Staff("lE HIEN", "NGA TU DIEN NGOC ", "THPT CHUYEN NGUYEN TRAI", 1000000000);
        System.out.println(staff1);
        System.out.println(staff2);
        System.out.println(staff3);

    }
}
